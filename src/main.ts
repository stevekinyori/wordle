import { exit } from 'process';

import { terminal } from 'terminal-kit';

import { Game } from './Game';
import { setup } from './Setup';
import { TerminalRenderer } from './Renderer';

terminal.clear();

setup().then((setup) => {
  const game = new Game(new TerminalRenderer(), setup.answer, setup.words);
  game.start().finally(() => {
    exit();
  });
});
